

var map, infoBubble;
function init() {
  var mapCenter = new google.maps.LatLng(48.30978575, 37.17038329);
  map = new google.maps.Map(document.getElementById('map-main'), {
    center: mapCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    // How zoomed in you want the map to start at (always required)
    zoom: 13,

    scrollwheel: false,


    // How you would like to style the map.
    // This is where you would paste any style found on Snazzy Maps.
    styles: [{}]
  });

  var marker = new google.maps.Marker({
    position: new google.maps.LatLng(48.30978575, 37.17038329)
  });


  var contentString =

    '<a class="b-map-bubble" href="#">' +
      '<div class="b-map-bubble__media">' +
        '<img class="b-map-bubble__img img-scale" src="assets/media/content/b-map/1.jpg" alt="foto">' +
       '</div>' +
      '<div class="b-map-bubble__main">' +
        '<div class="b-map-bubble__title text-primary">Торгівельний павільойон</div>' +
        '<div class="b-map-bubble__info"><i class="b-map-bubble__ic"><svg class="ic" width="24" height="24"><use xlink:href="../svg-symbols.svg#location"></use></svg></i>85612, Покровськ, вул. Центральна, 45</div>' +
        '<div class="b-map-bubble__info"><i class="b-map-bubble__ic"><svg class="ic" width="20" height="20"><use xlink:href="../svg-symbols.svg#attention-rounded"></use></svg></i>Без правовстановлюючих документів на земельну ділянку</div>' +
      '</div>' +
     '</a>' ;

  infoBubble = new InfoBubble({
    maxWidth: 440,
    content: contentString
  });

  infoBubble.open(map, marker);


}
google.maps.event.addDomListener(window, 'load', init);
